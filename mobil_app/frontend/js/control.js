var serverURL = 'http://192.168.100.102:3000'
var serverConnect = false
var registrationCorrect = false

var login = new Vue({
    el:"#login",
    data:{
        vis: true
    },
    methods: {
        show: function(){
            getURL.vis = false
            circleLoader.vis = false
            profilBody.vis = false
            helpBody.vis = false
            dataBody.vis = false
            registrationBody.vis = false
            profilBody_out.vis = false
            dataBody_out.vis = false
            loginBody.vis = true
        }
    }
})

var datas = new Vue({
    el:"#datas",
    data:{
        vis: true,
    },
    methods: {
        show: function(){
            getURL.vis = false
            circleLoader.vis = false
            profilBody.vis = false
            helpBody.vis = false
            loginBody.vis = false
            registrationBody.vis = false
            profilBody_out.vis = false
            dataBody_out.vis = false
            dataBody.vis = false

            // if connect is valid, then ask data/2s
            if(!serverConnect){
                circleLoader.vis = true
                setInterval(function(){
                    fetch(serverURL + '/actualData')
                    .then(async(resp) => {
                        dataBody.list = await resp.json()
                        dataBody.getData()
                        circleLoader.vis = false
                    })
                    /*
                    fetch(serverURL + '/resHysteresis')
                    .then(async(resp) => {
                        dataBody.list = await resp.json()
                        dataBody.getData()
                        circleLoader.vis = false
                    })*/
                }, 2000);
                dataBody.vis = true                
            }
            else dataBody_out.vis = true
        }
    }
})

var profil = new Vue({
    el:"#profil",
    data:{
        vis: true
    },
    methods: {
        show: function(){
            getURL.vis = false
            circleLoader.vis = false
            dataBody.vis = false
            helpBody.vis = false
            loginBody.vis = false
            registrationBody.vis = false
            dataBody_out.vis = false
            profilBody_out.vis = false
            profilBody.vis = false
            if (serverConnect) profilBody.vis = true
            else profilBody_out.vis = true
        }
    }
})

var help = new Vue({
    el:"#help",
    data:{
        vis: true
    },
    methods: {
        show: function () {
            getURL.vis = false
            circleLoader.vis = false
            profilBody.vis = false
            dataBody.vis = false
            loginBody.vis = false
            registrationBody.vis = false
            profilBody_out.vis = false
            dataBody_out.vis = false
            helpBody.vis = true
        }
    }
})

var registration = new Vue({
    el:"#registration",
    data:{
        vis: true
    },
    methods:{
        show: function(){
            getURL.vis = false
            circleLoader.vis = false
            profilBody.vis = false
            helpBody.vis = false
            dataBody.vis = false
            profilBody_out.vis = false
            dataBody_out.vis = false
            loginBody.vis = false
            registrationBody.vis = true
        }
    }
})

var urlChange = new Vue({
    el:"#urlChange",
    data:{
        vis: true
    },
    methods:{
        show: function(){
            registrationBody.vis = false
            circleLoader.vis = false
            profilBody.vis = false
            helpBody.vis = false
            dataBody.vis = false
            profilBody_out.vis = false
            dataBody_out.vis = false
            loginBody.vis = false
            getURL.vis = true
        }
    }
})

var navbar = new Vue({
	el:"#navbar_div",
	data:{
        navbarVis: true
    }
})

//------Bodys--------------------

var getURL = new Vue({
    el:"#getURL_div",
    data:{
        vis: true,
        inputUrl: ''
    },
    methods:{
        changeURL: function(){
            serverURL = 'http://'+this.inputUrl+':3000'
            getURL.vis = false
            loginBody.vis = true
        }
    }
})

var loginBody = new Vue({
    el:"#login_div",
    data:{
        vis: false,
        signEmail: 'example@gmail.com',
        signPassword: 'asd123',
        serverURL: serverURL
    }
})

var dataBody = new Vue({
    el:"#data_div_in",
    data:{
        vis: false,
        list: [],
        smoke: 'Normál',
        refreshed: null,

    },
    methods:{ 
        getData: () => {
            // if(this.list.smoke !== 0) smoke = 'Veszélyes';
            let date = new Date(Date.now());
            this.refreshed = date.getFullYear() + "." + date.getMonth() + "." + date.getDate() + " " + date.getTime()/3600/1000 + ":" + date.getTime()/60/1000 + ":" + date.getTime()/1000;
        }
    }
})

var dataBody_out = new Vue({
    el:"#data_div_out",
    data:{
        vis: false
    },
})

var profilBody = new Vue({
    el:"#profil_div_in",
    data:{
        vis: false
    },
})
var profilBody_out = new Vue({
    el:"#profil_div_out",
    data:{
        vis: false
    },
})

var helpBody = new Vue({
    el:"#help_div",
    data:{
        vis: false
    }
})

var registrationBody = new Vue({
    el:"#reg_div",
    data:{
        vis: false
    }
})

var circleLoader = new Vue({
    el:"#circle-loader_div",
    data:{
        vis: false
    }
})

//lenyíló menü elrejtése választás után
$('.navbar-collapse a').click(function(){
    $(".navbar-collapse").collapse('hide');
});

//bejelentkezés Form ablak adatainak küldése node-nak
$('#loginForm').submit(async (e) => {
    e.preventDefault();
    const response = await $.ajax({
        type: "POST",
        url: serverURL+'/login',
        data: $('#loginForm').serialize(),
        success: async (response) => {console.log(response)}
      })
      console.log(response) // TODO login
});

//regisztráció Form ablak adatainak küldése node-nak
$('#regForm').submit(async (e) => {
    e.preventDefault();
    const response = await $.ajax({
        url: serverURL+'/registration',
        type:'POST',
        data:$('#regForm').serialize(),
    });
    console.log(response);
});