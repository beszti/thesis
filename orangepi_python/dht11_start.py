from pyA20.gpio import gpio
from pyA20.gpio import port
from flask import Flask, jsonify
from env import FLASK_PORT
import json

#import RPi.GPIO as GPIO
import dht11

# initialize GPIO
PIN = port.PA6
gpio.init()

# initialize flask server
app = Flask(__name__)

@app.route("/datas")
def datas():
    # read data using pin 13
    data = sensorData()
    return jsonify(data)

@app.route("/hello")
def hello():
    return "Hello"

# initialize data reading
def sensorData():
    instance = dht11.DHT11(pin=PIN)
    result = instance.read()
    if result.is_valid():
        data = {"temperature": result.temperature, "humidity": result.humidity}
        return data

app.run(host='0.0.0.0', debug=False, port=FLASK_PORT)
